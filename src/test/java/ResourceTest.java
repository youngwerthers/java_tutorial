import java.io.File;
import java.net.URL;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by daisy.kang on 15. 9. 29..
 */
public class ResourceTest {

  @org.junit.Test
  public void readMainResource() {
    URL resource = this.getClass().getResource("hello.txt");
    assertNotNull(resource);

    String fileName = resource.getFile();
    System.out.println(fileName);
    File file = new File(fileName);
    System.out.println(file.getAbsolutePath());
  }
}

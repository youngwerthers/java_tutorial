package generic;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Generics :
 *    - 컴파일 시점에 버그를 발견할 수 있게 하여 코드에 안정성을 높인다.
 *
 * @link https://docs.oracle.com/javase/tutorial/java/generics/index.html
 * @link http://ohgyun.com/51 // generic 소개
 */
public class GenericTest {

  /**
   * 왜 Generic을 사용하는가?
   *
   *  - generics(포괄적)는 class나 interface, method를 정의할때 파라미터로써의 types을 가능하게 한다.
   *  - 일반적인 파라미터(메서드 선언시)는 값으로 사용되지만, type 파라미터는 type으로 쓰인다.
   *  - 다른 input인데도, 동일한 코드를 재사용하게 해준다.
   *
   *  - 장점
   *    컴파일 시점에 타입 체크
   *    cast 제거
   *    다른 type들의 집합에서 generic 알고리즘을 구현할수 있게 한다. 커스터마이징 가능, type safe, 읽기 쉽다.
   */
  public void introduceGeneric() {
    // non-generic : casting이 필요하다.
    List list = new ArrayList();
    list.add("hello");
    String v1 = (String)list.get(0);

    // generic: casting 불필요
    List<String> list2 = new ArrayList<>();
    list2.add("hello");
    String v2 = list2.get(0);
  }

  /**
   * Non-Generic Class
   *
   * primitive type이 아닌 모든것을 컴파일 시점에 넘길 수 있다.
   * 단, Integer를 넣고, String으로 꺼내는 경우 런타임 시점에야 에러를 발견할 수 있다.
   */
  public class NonGenericBox {
    private Object object;

    public Object getObject() {
      return object;
    }

    public void setObject(Object object) {
      this.object = object;
    }
  }

  @Test
  public void nonGenericTest() {
    // 정상적인 케이스
    NonGenericBox b = new NonGenericBox();
    b.setObject("stringValue");
    assertEquals("stringValue", b.getObject());

    b.setObject(100);
    assertEquals(100, b.getObject());

    // 런타임 시점에 오류를 알 수 있다.
    try {
      b = new NonGenericBox();
      b.setObject("stringValue");

      // java.lang.ClassCastException
      assertEquals(new Integer(100), (Integer)b.getObject());
    } catch (Exception e) {
      assertNotNull(e);
      e.printStackTrace();
    }
  }

  /**
   * Generic Class
   *
   * Type variable:
   *   - <T1, T2, T3, ..., Tn> 으로 사용 가능
   *   - class 안의 어디에서도 사용 가능함
   *   - non-primitive type이면 아무거나 가능, 예) class, interface, array, 등
   *
   *  네이밍 컨벤션
   *     - 1글자, 대문자
   *     - 예) 일반적인 이름
   *       E - Element (used extensively by the Java Collections Framework)
   *       K - Key
   *       N - Number
   *       T - Type
   *       V - Value
   *       S,U,V etc. - 2nd, 3rd, 4th types
   */
  public class GenericBox<T> {

    private T t;

    public T get() {
      return t;
    }

    public void set(T t) {
      this.t = t;
    }
  }

  /**
   *  Generic 클래스를 참조하기 위해서는
   *  구체적인 값을 T 자리에 넣으면 된다.
   *
   *  예) Box<Integer> integerBox;
   */
  @Test
  public void genericTest() {
    // 정상적인 상황

    // 실제 GenericBox Object가 생성되는 것이 아니고
    // 단지 String을 담은 GenericBox 참조를 들고 있을 거라는 선언이다.
    // 즉, GenericBox<String>를 읽는 방법
    // 일반적으로 parameterized type으로 알려져 있다.
    GenericBox<String> stringBox;

    // 이 클래스를 초기화 하기 위해 아래와 같이 해야한다.
    stringBox = new GenericBox<String>();
    stringBox.set("stringValue");
    String stringValue = stringBox.get();
    assertEquals("stringValue", stringValue);

    // Java SE 7 이후부터,
    // 컴파일 시점에 결정할 수 있게끔 Diamond<> 초기화 가능
    // @link https://docs.oracle.com/javase/tutorial/java/generics/genTypeInference.html
    GenericBox<Integer> integerBox = new GenericBox<>();
    integerBox.set(100);
    Integer integerValue = integerBox.get();
    assertEquals(new Integer(100), integerValue);

    // 잘못된 대입
    GenericBox<Integer> integerBox2 = new GenericBox();
    // 컴파일 시점에 casting error 확인 가능함
//      integerBox2.set("stringValue");
    Integer integerValue2 = integerBox.get();
  }

  /**
   * @TODO: 아래 둘의 차이는?
   *
   * Type Parameter :
   *    Foo<T>
   *
   * Type Argument :
   *    Foo<String>
   *
   */


  /**
   * Generic Class : Multiple Type Parameters
   */
  public interface MultipleTypeGenericBox<K, V> {
    public K getKey();
    public V getValue();
  }

  public class MultipleTypeGenericBoxImpl<K, V> implements MultipleTypeGenericBox<K, V> {
    private K key;
    private V value;

    public MultipleTypeGenericBoxImpl(K key, V value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public K getKey() {
      return this.key;
    }

    @Override
    public V getValue() {
      return this.value;
    }

  }

  @Test
  public void multipleGenericBoxTest() {

    // autoboxing으로 인해 integer대신 int를 넘겨도 된다.
    int intValue = 100;

    // @link: https://docs.oracle.com/javase/tutorial/java/data/autoboxing.html
    // k를 String으로 v를 Integer로 초기화 한다.
    MultipleTypeGenericBoxImpl<String, Integer> b1 = new MultipleTypeGenericBoxImpl<String, Integer>("price", intValue);


    // 컴파일 시점에 에러 발생
    // MGenericBox<String, Integer> b2 = new MGenericBox<>("price", "100");

    // java 컴파일러가 추론하므로, Diamond 노테이션으로 줄여서 가능하다.
    MultipleTypeGenericBoxImpl<String, String> b3 = new MultipleTypeGenericBoxImpl<>("price", "100");
  }

  /**
   * Parameterized Types
   *
   * type parameter(예: K, V)를 paramiterized type(예: List<String>)으로 대체할 수 있다.
   */
  @Test
  public void paramiterizedTypeTest() {
    List<String> arrayList = new ArrayList<>();
    arrayList.add("a1");

    //
    MultipleTypeGenericBoxImpl<String, List<String>> b = new MultipleTypeGenericBoxImpl<>("key", arrayList);
  }


  /**
   * Raw Types:
   *    type argument가 없는 generic class 또는 interface를 말한다.
   *    raw type은 generic 이전인 JDK 5.0 이전까지의 Collection class 같은 api class에서 많이 볼 수 있다.
   */
  @Test
  public void rawTypeTest() {
    // GenericBox<T>의 paramterized type을 생성하기 위해서는 실제 type을 넣어야함
    GenericBox<Integer> intBox1 = new GenericBox<>();
    // intBox.set("10"); -> type check compile error
    intBox1.set(10);
    assertEquals(0, Integer.compare(10, intBox1.get()));

    // 만일 actual type을 생략한다면 GenericBox<T>의 rawType을 생성할 수 있다.
    GenericBox rawBox1 = new GenericBox();
    rawBox1.set(1);
    assertEquals(1, rawBox1.get());
    rawBox1.set("hi");
    assertEquals("hi", rawBox1.get());
  }

  /**
   * parameterized type 을 raw type에 assign: 허용
   */
  @Test
  public void assignParameterizedTypeToRawType() {
    GenericBox<String> stringBox = new GenericBox<>();

    // OK
    GenericBox rawBox = stringBox;

    stringBox.set("hi");
    assertEquals("hi", rawBox.get());
  }

  /**
   * raw type 을 parameterized type에 대입하는 경우, warning이 발생한다.
   * 이유는 타입체크를 런타임 시점까지 미루기 때문
   * 따라서 raw type 사용을 피해야한다.
   *
   * Type Erasure 섹션에서 Java 컴파일러가 raw type을 사용하는 방법을 더 소개한다.
   */
  @Test
  public void assignRawTypeToParameterizedType() {
    GenericBox<Integer> intBox = new GenericBox<>();
    GenericBox rawBox = new GenericBox();

    // warning: unchecked conversion
    intBox = rawBox;

    // warning: unchecked invocation to set(T)
    rawBox.set(0);
    assertEquals(0, rawBox.get());

    // rawBox에 string을 넣고, intBox에서 꺼내는 경우 -> 런타임 에러 발생
    rawBox.set("10");
    try {
      Integer intVal = intBox.get();    // java.lang.String cannot be cast to java.lang.Integer
    } catch (ClassCastException e) {
      assertNotNull(e);
      e.printStackTrace();
    }
  }

  /**
   *
   * Note: Example.java uses unchecked or unsafe operations.
   * Note: Recompile with -Xlint:unchecked for details.
   */
  @Test
  public void uncheckedErrorMessage() {
    GenericBox<Integer> integerBox;
    integerBox = createBox();
  }

  private GenericBox createBox() {
    return new GenericBox();
  }

  public class Node {
    public Object data;

    public Node(Object data) { this.data = data; }

    public void setData(Object data) {
      System.out.println("Node.setData");
      this.data = data;
    }
  }

  public class MyNode extends Node {

    public MyNode(Integer data) { super(data); }

//    @Override
    public void setData(Integer data) {
      System.out.println("MyNode.setData(integer)");
      super.setData(data);
    }
  }

  @Test
  public void bridgeMethodTest() {
    MyNode mn = new MyNode(5);
    Node n = (MyNode)mn;    // A raw type - compiler throws an unchecked warning
    n.setData("Hello");     // 런타임 에러 ->

    // 컴파일 시점에 에러 확인
    // erasure 과정을 통해 bridge method가 생성됨으로써
    // Integer x = (String)mn.data;
  }
}
